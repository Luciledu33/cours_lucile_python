# Lucile Python

Ici seront déposés tous les supports de cours liés au cours de Python.

### ➜ [**Cours**](./cours/README.md)

- [Introduction aux "bonnes pratiques" de l'informatique](./cours/cours/intro/README.md)
- [Principe du moindre privilège](./cours/cours/least_privilege_principle/README.md)
- [Introduction a Python](./cours/cours/intro_python/README.md)

#### ➜ [**Notions**](./cours/notions/README.md)

- [Git](./cours/notions/git/README.md)

### ➜ [**TP**](./tp/README.md)

- [TP1 : Bases python](./tp/1/README.md)

### ➜ [**TP Lucile**](./tp-Lucile/README.md)

- [TP1 : Bases python](./tp-Lucile/1/README.md)

### ➜ [**corrections TP**](./tp-corrections/README.md)

- [TP1 : Bases python](./tp-corrections/1/README.md)
