
# Intro à Python

## Sommaire

- [Intro à Python](#intro-à-python)
    - [sommaire](#sommaire)
    - [Python et la philosophie Python](#python-et-la-philosophie-python)
    - [Installation](#installation)
    - [Valeurs expressions et variables](#valeurs-expressions-et-variables)
        - [Valeurs et expressions](#valeurs-et-expressions)
            - [Nombres entiers (int)](#nombres-entiers)
            - [Nombres flottants (float)](#nombres-flottants)
            - [Booléens (bool)](#booléens)
            - [Chaînes de caractères (str)](#chaînes-de-caractères)
        - [Variables](#variables)
            - [Langages statiquement et dynamiquement typés](#langages-statiquement-et-dynamiquement-typés)
        - [Communiquer avec le monde extérieur](#communiquer-avec-le-monde-extérieur)
            - [Saisie](#saisie)
            - [Affichage](#affichage)
    - [Enchainements d'instructions](#enchainements-d'instructions)
        - [Séquence](#séquence)
        - [Condition](#condition)
        - [Répétition](#répétition)
            - [La boucle while](#la-boucle-while)
            - [La boucle for](#la-boucle-for)
            - [Boucles avec range](#boucles-avec-range)
        - [Fonction](#fonction)
            - [Appel de fonction](#appel-de-fonction)
            - [Types particuliers de fonction](#types-particuliers-de-fonction)
        - [Variables de fonction](#variables-de-fonction)
            - [Portée des variables](#portée-des-variables)
    - [Tableaux](#tableaux)
        -[Déclarer un tableau](#déclarer-un-tableau)
        - [Opérations sur les tableaux](#opérations-sur-les-tableaux)
        - [Paramètres de type tableau](#paramètres-de-type-tableau)
        - [Tableaux et mutabilité](#tableaux-et-mutabilité)
        - [Mutabilité égalité et identité](#mutabilité-égalité-et-identité)
        - [Tableaux de chaînes de caractères](#tableaux-de-chaînes-de-caractères)
    - [Récursivité](#récursivité)

## Python et la philosophie Python

Pour présenter le rapidement, python est un language de programmation multifonction, multi-paradigme avec un typage dynamique. (c'est technique et bien qu'intéressants pas très utiles à notre niveau de comprendre tout ça. Tu peux googler si tu as du temps c'est toujours bon)
Le typage dynamique en revanche lui a son importance car cela veut dire qu'une variable peut changer de type a n’importe quel moment ! 
Python c'est un language **Clair et Concis**. Il a l'autre avantage d'avoir 30 ans. Alors autant pour nous, on n'a pas hate.. mais qu'un language de programmation soit mature c'est une bonne chose car cela induit qu'il est complet et stable et c'est pratique pour de ! ☝️

Niveau "politique" python c'est un language Open-source. C'est donc gratuit, libre d'accès et tout le monde peut y ajouter des nouveautés. (Décision par votes de PEP on en reparle juste après 😌)

C'est Guido van Rossum qui l'a créé et participer à la maintenance pendant 30 ans...

Python se veut donc etre un language **LISIBLE** car comme dit dans lintro aux bonnes pratiques](./intro/README md) on passe plus de temps à lire dur code pour la correction de bug, la maintenance, la documentation, l'ajout de fonctionnalités, etc..

L'objectif est donc en python d'avoir une certaine efficacité de codage.
Pour cela le programmeur s'occupe des fonctionnalités et python s'occupent de l'implementation et de l'allocation de mémoire.

Python gère aussi des structures évolué de données comme les tableaux à taille variable, les listes, les FIFO.. (une sorte de file d'attente)

Avec python on peut faire: 
- des sites Web
    - Front-end en HTML/CSS/JAVASCRIPT.
    - Backend Python avec Flask par exemple. (Les sites instagram, Youtube, pinterset ou mozilla fonctionnent avec Python)
- des logiciels (Bittorrent ou DropBox sont d'en Python)
- Big Data (gestion de grandes bases de données)
- Machine learning (Intelligence Aritificiel)
- Domotique (frigo connecté entre autres 😂 )

Bref du coup tu comprends bien qu'on peut tout faire avec Python ! 🤗

Python c'est aussi une énorme communauté. Tout le monde peut proposer des packages Python ( il y en a plus de 200 000 !)
Il y a énormément de documentation sur internet. C'est génial car au moindre problème une recherche google et tu as des réponses.
Tu as également la fonction `help` dans ton terminal qui te donne des infos. Avec un logiciel comme VScode tu as aussi des aides que ce soit pour l'indentation ou même sur l'utilisation des fonctions Python.

Tout cela fait que Python est **LE LANGUAGE** depuis quelques années. Il est donc bien pertinent de se familiariser à son utilisation. D'autant plus pour toi qui aimerais devenir devlopeur full-stack !

![python_change](./pics/python_change.png)

Un fichier Python c'est tout simplement un `fichier.py`
et ça se lance depuis un terminal comme ça : `python3 fichiers.py`

La philosophie Python est résumée dans la PEP20 sous 18 commandements: 

- Beautiful is better than ugly.
- Explicit is better than implicit.
- Simple is better than complex.
- Complex is better than complicated.
- Flat is better than nested.
- Sparse is better than dense.
- Readability counts.
- Special cases aren't special enough to break the rules.
- Although practicality beats purity.
- Errors should never pass silently.
- Unless explicitly silenced.
- In the face of ambiguity, refuse the temptation to guess.
- There should be one-- and preferably only one --obvious way to do it.
- Although that way may not be obvious at first unless you're Dutch.
- Now is better than never.
- Although never is often better than *right* now.
- If the implementation is hard to explain, it's a bad idea.
- If the implementation is easy to explain, it may be a good idea.
- Namespaces are one honking great idea -- let's do more of those!

Pour avoir une vision parfaite des bons pratiques pythons je te conseille de lire le PEP8 ici https://pep8.org/

Bon assez parlait de Python parle Code en Python ! 🤓

## Installation

De base sur Mac il y a déjà Python. **FIN ?**

Bon c'est Python 2.7 alors on va quand même installer la dernière version. 🤗
Alors let's go ! [ici](https://www.python.org/downloads/)
- Tu cliques sur download Python 3.10.2
- Tu ouvres le fichier pkg que tu viens de télécharger. Tu te laisses guider et tout roules-tu aura dans ton dossier application un dossier python 3.10.2
- maintenant tu créer un nouveau dossier dans lequel tu rangeras ton code.
- Dans VScode tu ouvres ce dossier, tu créais un fichier avec le nom que tu veux et tu finis par .py `cequetuveux.py`
- Si tu n’as jamais programmé en Python dans VScode il te proposera sans doute d'installer des choses. Installe-les (c'est ce qui permettra de corriger les erreurs spécifiques à Python)

On est pret à commencer ! 
Pour la suite je te conseille de tester dans ton VScode pour te familiariser avec les différentes bases de python. 👩‍💻 

## Valeurs expressions et variables
[sommaire](#sommaire)

Dans cette partie, on va décrire les opérations de base supportées par le langage Python. Fondamentalement, un ordinateur sert à effectuer des calculs, mais on va voir que ce terme est à prendre au sens large. Après avoir présenté les types de valeur supportés par Python et les calculs qu'il peut effectuer avec ces valeurs, on verra comment les résultats de ces calculs sont stockés dans la mémoire de l'ordinateur.

### Valeurs et expressions

Dans cette section, nous présentons les valeurs que nous allons utiliser dans nos algorithmes, et les calculs possibles sur ces valeurs. Nous regroupons sous le terme expression les valeurs simples et les calculs plus ou moins complexes. Tous les exemples d'expressions de cette section peuvent être tapés directement dans VScode qui affichera le même résultat.

#### Nombres entiers

Python supporte, comme tous les langages de programmation classiques, le calcul sur les nombres entiers (en anglais : integer, abrégé par int). Un nombre entier s'écrit comme une série de chiffres, éventuellement précédée par un signe (+ ou -). Par exemple : 0, 42, +123 et -987654 sont des entiers.

Python supporte les opérations arithmétiques usuelles sur les entiers :

```py
>>> 10+3   # addition
13
>>> 10-3   # soustraction
7
>>> 10*3   # multiplication
30
>>> 10//3  # division entière
3
>>> 10%3   # modulo
1
>>> 10**3  # puissance
1000
```

> Le modulo c'est l'opération qui permet de calculer le reste de la division euclidienne (ou division entière). Par exemple, 9%2=1 car 9 = 2*4 + 1, 4 étant la valeur obtenue par la division entière 9//2.

Ces opérateurs peuvent bien sûr être combinés. Python respecte les règles de priorité habituelles entre opérateurs, et autorise l'utilisation des parenthèses pour les surcharger. Les espaces peuvent aussi être utilisés pour améliorer la lisibilité :

```py
>>> 5+4*3**2
41
>>> 5  +  4 * 3**2
41
>>> ((5+4)*3)**2
729
>>> ((5+4) * 3)**2
729
```

> Pour mémoire, il existe un ordre de priorité entre les opérateurs arithmétiques : les parenthèses sont les plus prioritaires, viennent ensuite les exposants, la multiplication et la division, puis l'addition et la soustraction. N'hésitez pas à utiliser les parenthèses pour forcer les priorités et rendre vos algorithmes plus lisibles !

Notons quelques spécificités de Python qui le distinguent d'autres langages de programmation (comme C ou Java) :

- L'opérateur de division entière `//` est différent de l'opérateur de division réelle `/` (qu'on présentera dans la section suivante). Dans d'autres langages, il n'existe souvent que l'opérateur `/` et c'est le contexte qui détermine le type de division qu'il représente. Le fait d'avoir deux opérateurs distincts améliore la lisibilité des programmes.

- Alors que dans la plupart des langages de programmation, les entiers sont limités en taille, Python peut faire des calculs sur des entiers arbitrairement grands (pour s'en convaincre, on pourra taper `123**456`). Si cela entraîne de moins bonnes performances, cela permet en revanche de se focaliser sur les aspects algorithmiques et calculatoires, en s'affranchissant des limites imposées par d'autres langages.


#### Nombres flottants

Au delà des entiers, les ordinateurs sont capables de représenter des nombres à virgule. Ils utilisent pour cela un système nommé nombre à virgule flottante ou encore nombre flottant, ou tout simplement flottant (en anglais : floating point number, abrégé par `float`).

Sous leur forme la plus simple, les flottants sont composés d'une partie entière composée de chiffres, suivie du point décimal `.`, suivi d'une partie décimale composée de chiffres, le tout éventuellement précédé d'un signe `+` ou `-`. Par exemple : `0.0`, `4.2`, `+12.3`, ou `-987.654` sont des flottants.

Mais ils peuvent également être suivis d'un exposant, formé par la lettre `e` suivi d'un nombre entier. Cette notation correspond à la notation scientifique (le `e` signifiant « multiplié par 10 puissance... »). Par exemple, `1e2` équivaut à `100.0`, `-3.4e+5` équivaut à `-340000.0`, et `6.7e-8` équivaut à `0.000000067.

Là encore, Python supporte les opérations arithmétiques de base sur les nombres flottants :

```py
>>> 2.5 + 1.5    # addition
4.0
>>> 2.5 - 1.5    # soustraction
1.0
>>> 2.5 * 1.5    # multiplication
3.75
>>> 2.5 / 1.5    # division réelle
1.6666666666666667
>>> 2.5 ** 1.5   # puissance
3.952847075210474
```

Notons que toutes ces opérations (y compris la division réelle) peuvent combiner entiers et flottants, mais le résultat sera alors toujours un flottant, même si sa partie décimale est 0.

Notons enfin que, contrairement au calcul sur les entiers, le calcul sur les flottants est un calcul approché : certains nombres ne peuvent pas être représentés de manière exacte par des flottants, comme 1/3 (qui n'a d'ailleurs pas non plus d'écriture décimale exacte) mais aussi 1/10 ou 1/5 (ce qui peut sembler plus surprenant) :

```py
>>> 0.1 + 0.2
0.30000000000000004
```

#### Booléens

On est souvent amené dans les algorithmes à manipuler des valeurs de vérité, nommées booléens (en anglais boolean, abrégé par bool). Il n'existe que deux valeurs booléennes : True et False. Les opérateurs sur les booléens sont les opérateurs logiques and, or et not :

```py
>>> False and True
False
>>> False or True
True
>>> not False
True
```
L'intérêt des valeurs booléennes réside notamment dans la possibilité de les produire en comparant des valeurs d'autres types, puis de combiner éventuellement ces résultats avec les opérateurs logiques :

```py
>>> 1+1 == 2    # égalité
True
>>> 1+1 != 2    # différence
False
>>> 3.3 < 10/3  # inférieur strict
True
>>> 3.3 > 10/3  # supérieur strict
False
>>> 1+1 <= 2    # inférieur ou égal
True
>>> 1+1 >= 2    # supérieur ou égal
True
>>> (1+1 == 2) and (3.3 > 10/3)
False
```

> On remarque ci-dessus que le test d'égalité utilise l'opérateur `==`, et non l'opérateur usuel `=`. On verra ci-dessous dans la section [Variables](#variables) que ce dernier a un autre sens en Python (ainsi que dans la quasi-totalité des langages de programmation).

Comme les opérateurs numériques, les opérateurs booléens et les opérateurs de comparaison ont des priorités. Pour une liste exhaustive des priorités en python, jette un coup d'oeuil a la [documentation Python](https://docs.python.org/3/reference/index.html), tu y trouvera toutes les réponses.

#### Chaînes de caractères

Python ne se limite pas aux valeurs purement mathématiques. Il peut également manipuler du texte, sous forme de chaînes de caractères ou tout simplement chaînes (en anglais : character string, abrégé par `str). Une chaîne est une séquence de caractères de longueur arbitraire. Python supporte le codage UNICODE, ce qui permet d'utiliser un large éventail de caractères, incluant les caractères accentués du français.

Une chaîne de caractères est délimitée par les symboles `'` ou `"`. Par exemple `'hello Lucile` ou `"bonjour toi"` sont des chaînes de caractères.

```py
>>> len("Lucile")  # longueur
6
>>> "Lucile" + "bientot pro en Python"   # concaténation
'Lucilebientot pro en Python'
>>> "Lucile"[0]    # premier caractère
'L'
>>> "Lucile"[:3]   # sous-chaîne contenant les 3 premiers caractères
'Luc'
>>> "Lucile"[3:]   # sous-chaîne commençant au 4ème caractère
'ile'
```

Les exemples ci-dessus méritent quelques explications :

- La longueur d'une chaîne prend en compte tous les caractères, y compris les espaces.
- La concaténation n'insère pas d'espace ; pour obtenir le résultat `'Lucile bientot pro en Python'` ci-dessus, il aurait fallu écrire (par exemple) `"Lucile" + " bientot pro en Python"` (espace placé avant la lettre m ☝️).
- Contrairement à d'autres langages de programmation, Python n'a pas de type caractère distinct du type chaîne ; ainsi, la troisième valeur ci-dessus est simplement une autre chaîne, de longueur 1.
- Comme dans la plupart des langages de programmation, les chaînes de caractères sont indicées à partir de 0, donc le premier caractère a l'indice 0, le deuxième a l'indice 1, etc.
- En Python, lorsque l'on sélectionne un sous ensemble d'une chaîne à l'aide de bornes, la borne inférieure est incluse, et la borne supérieure est exclue. Par exemple, lorsque l'on écrit `"le monde"[3:5]`, on va sélectionner la sous-chaîne commençant à l'indice 3 et se terminant à l'indice 4, c'est-à-dire `"mo". Lorsque la borne inférieur n'est pas précisée, on commence au début de la chaîne, et lorsque la borne supérieure n'est pas précisée, on va jusqu'à la fin de la chaîne.

Les chaînes de caractères peuvent également être comparées avec les opérateurs de comparaison vus plus haut:

```py
>>> "le monde" == "le " + "monde"
True
>>> "le monde" != "le monde "  # les espaces comptent
True
>>> "bonjour" < "bonsoir"
True
>>> "bon" < "bonjour"
True
>>> "Z" < "a"
True
>>> "é" >= "z"
True
```

On voit dans les exemples ci-dessus que l'ordre induit par les opérateurs de comparaison (`<`, `>`, `<=` et `>=`) sur les chaînes de caractères n'est pas totalement intuitif. Il se rapproche de l'ordre alphabétique, mais place les lettres majuscules avant les lettres minuscules. Par ailleurs, il ne place pas correctement les lettres accentuées. Il est donc à utiliser avec précaution.

Enfin, notons que les chiffres sont des caractères valides ; il ne faut cependant pas confondre les chaînes de caractères composées de chiffres avec les nombres correspondants:

```py
>>> "123" == 123
False
>>> "1" + "1"   # concaténation, et non addition
'11'
>>> "10" < "2"  # ordre alphabétique
True
```

### Variables

Maintenant qu'on a vu les types de valeur que nos algorithmes allaient pouvoir manipuler, on va voir comment ces valeurs sont stockées en mémoire. La notion centrale est celle de variable.

> emplacement de la mémoire de l'ordinateur, muni d'un nom et contenant une valeur qui peut changer (varier) au fil du temps
> opération consistant à fixer ou changer la valeur d'une variable

L'opérateur d'affectation en Python est l'opérateur `=` (comme dans la majorité des langages de programmation). À gauche de l'opérateur d'affectation, on indique le nom de la variable à affecter2. À droite, on donne une expression dont la valeur résultante est affectée à la variable :

```py
>>> message = "Coucou Lucile"
>>> message
'Coucou Lucile'
```

Après sa première affectation, une variable peut être utilisée dans une expression. Son nom est simplement remplacé par sa valeur :

```py
>>> a = 42
>>> a+1
43
>>> a = a+1
>>> a
43
```
Dans l'exemple ci-dessus, la variabl `a` se voir d'abord affecter la valeur `42`, donc `a+1` vaut `43`. On affecte alors l'expression `a+1` à la variable `a`, dont la nouvelle valeur est donc `43`.

#### Langages statiquement et dynamiquement typés

On distingue deux familles de langages de programmation qui diffèrent au niveau du typage des variables. Python appartient à la famille des langages dynamiquement typés. Dans ces langages, seules les valeurs sont typées ; par exemple, 42 est de type entier, "toto" est de type chaîne de caractères. Les variables, en revanche, n'ont pas de type. Ainsi dans ces langages, une variable peut en théorie contenir à des moments différents des valeurs de types différents. En pratique, cependant, on évite souvent cela, car cela nuit à la lisibilité du programme.

L'autre famille, celle des langage statiquement typés, inclue des langages comme le C ou Java. Dans ces langages, toute variable est associée à un type, et ne peut contenir que des valeurs de ce type. En imposant au programmeur d'exprimer ces contraintes, ces langages sont plus rigides que leurs homologues dynamiquement typés, mais peuvent en contrepartie détecter plus facilement certaines erreurs. C'est notamment le cas pour les pré/post-conditions concernant le typage des paramètres, qui sont vérifiées à la compilation, et donc garanties au moment de l'exécution. Cependant, le respect des autres pré/post-conditions reste de la responsabilité du programmeur.

> Dans les langage statiquement typés, une conséquence du typage des variables est la nécessité de déclarer une variable avant de l'utiliser. Cette contrainte n'existe pas dans les langages dynamiquement typés comme Python; la première affectation fait office de déclaration. Toute utilisation d'une variable non affectée créera une erreur à l'exécution.

### Communiquer avec le monde extérieur

Dans un programme interactif, il est nécessaire d'échanger des informations avec le monde extérieur, et en particulier l'utilisatrice ou l'utilisateur. On présente ici une manière simple de lui faire saisir une valeur, ou de lui afficher une valeur. Il existe bien sûr des manières plus sophistiquées et plus ergonomiques (utilisant toutes les fonctionalités des interfaces graphiques modernes) mais elles sortent du cadre de ce cours.

![input_output](./pics/input_output.png)

#### Saisie

Pour renseigner une variable avec une valeur saisie par l'utilisateur, on écrit une affectation avec à droite du signe `=` l'expression `input()` :

```py 
>>> val = input()
hello world        ← ce texte est saisi par l'utilisateur
>>> val
'hello world'
```

Une chaîne de caractères peut-être placée entre les parenthèses de `input()` pour afficher un message à l'utilisateur, indiquant ce qu'on attend de lui ou d'elle :

```py 
>>> nom = input("Quel est votre nom ? ")
Quel est votre nom ? Alex
>>> nom
'Alex'
>>> nom = input("Quel age avez vous ? ")
Quel age avez vous ? 23
>>> age
'23'
```
Comme on le voit ci-dessus, la valeur retournée par `input()` est toujours une chaîne de caractères (même si, par exemple, l'utilisateur ne saisit que des chiffres). Si on souhaite faire saisir une valeur d'un autre type (`int`, `float` ou `bool`), il faut le préciser comme dans l'exemple ci-dessous :

```py 
>>> age = int(input("Quel age avez-vous ? "))
Quel age avez-vous ? 23
>>> age
23
```

#### Affichage

En Python, toute information peut-être affichée avec l'inscruction print(). On place entre les parenthèses l'expression (ou les expressions, séparées par des virgules) à afficher :

```py 
>>> print("Merci")
Merci
>>> print("Alors comme ça", nom, ", vous avez", age, "ans ?")
Alors comme ça Alex , vous avez 23 ans ?
```
## Enchainements d'instructions
[sommaire](#sommaire)

OK, On sait manipuler des variables. Maintenant comment on en fait un algorithme ?
Un algorithme est composé d'une série d'instructions, qui peuvent être enchaînées ou regroupées de différentes manières.

### Séquence

La forme la plus simple d'enchaînement d'instructions composant une fonction est la séquence. Les instructions sont écrites l'une après l'autre, séparées par un saut de ligne. Pour Python, il est indispensable qu'elles soient toutes au même niveau d'indentation, c'est-à-dire précédées du même nombre d'espaces1. Les instructions d'une séquence sont toutes exécutées, dans l'ordre ou elles sont écrites.

Par exemple l'algorithme suivant, calculant les trois premières puissances d'un nombre :

```py
"""
:entrée x:  float, SAISI au clavier
:pré-cond:  Ø
:sortie p2: float, AFFICHÉ à l'écran
:sortie p3: float, AFFICHÉ à l'écran
:sortie p4: float, AFFICHÉ à l'écran
:post-cond: p2 = x², p3 = x³, p4 = x⁴
"""
x = float(input("x="))
p2 = x*x
p3 = p2*x
p4 = p3*x
print(p2, p3, p4)
```
On peut représenter l'enchaînement des instructions de cet algorithme par le diagramme ci-dessous :
![Diagramme Sequence](./pics/diagramSequence.png)

> La spécification ne fait pas partie de l'algorithme (elle décrit le « quoi », pas le « comment »). On l'encadre par des triples guillemets (`""") pour indiquer à Python qu'il peut ignorer cette partie lorsqu'il exécute l'agorithme.

### Condition

Dans certains cas, il est nécessaire d'exécuter des instructions différentes selon qu'une condition est remplie ou non. Dans ce cas, on utilisera un enchaînement conditionnel. Celui-ci est composé ainsi :
- première ligne : 
    - le mot-clé `if`,
    - l'expression booléenne représentant la condition,
    - le caractère `:`.
- enchaînement d'insructions à exécuter si la condition est vraie
    - toutes avec un niveau d'indentation supérieur à la première ligne.
- le mot-clé `else:`
    - au même niveau d'indentation que la première ligne.
- enchaînement d'instructions à exécuter si la condition est fausse
    - toutes avec un niveau d'indentation supérieur à la première ligne.

Les enchaînements suivant le `if` et le `else` ne sont bien sûr pas limités à des enchaînements séquentiels. Il peuvent comporter d'autres enchaînements conditionnels, ou des enchaînements répétitifs (cf. ci-dessous). La première instruction précédée du même nombre d'espaces (ou moins) que la première ligne (la ligne du if) sera reconnue comme ne faisant pas partie de l'enchaînement conditionnel. Elle sera donc exécutée que la condition soit vraie ou non.
Considérons par exemple l'algorithme suivant, qui calcule pour un nombre donné sa valeur absolue et son signe (représenté par le nombre 1 ou -1) :

```py
"""
:entrée x:      float, AFFECTÉ précédemment
:pré-cond:      Ø
:sortie signe:  int,   AFFICHÉ à l'écran
:sortie valabs: float, AFFICHÉ à l'écran
:post-cond:     signe = +1 si x ≥ 0, -1 sinon
:post-cond:     valabs = |x|
"""
if x >= 0:
    signe = +1
    valabs = x
else:
    signe = -1
    valabs = -x
print(signe, valabs)
```

On peut représenter l'enchaînement des instructions de cet algorithme par le diagramme ci-dessous :
![diagramme condition](./pics/diagramCondition.png)

#### Condition multiples 

Afin d'éviter d'imbriquer les `if` et les `else` les uns dans les autres, Python propose l'instruction `elif` (contraction de else if) qui se traduit en langage courant par "sinon si".

Le `elif` doit être utilisé avec précaution et parcimonie, uniquement lorsque l'on est certain que toutes les conditions sont exclusives. En effet, les conditions sont vérifiées les unes après les autres jusqu'à ce qu'une condition soit vraie. Si une condition est vérifiée, les suivantes dans la liste ne sont pas examinées.

Une suite de `elif` peut (mais doit) se terminer par un `else` qui sera donc traité si aucune condition n'a été vérifiée jusque là.

Voici un exemple d'utilisation du `elif`, qui donne le nombre de jours dans le mois donné (pour une année non bissextile) :

```py
"""
:entrée mois: int, AFFECTÉ précédemment
:pré-cond:    1 ≤ mois ≤ 12
:sortie nbj:  int, AFFECTÉ pour la suite
:post-cond:   nbj est le nombre de jours du mois dont le numéro est donné
"""
if mois == 2:
    nbj = 28
elif mois == 4 or mois == 6 or mois == 9 or mois == 11:
    nbj = 30
else:
    nbj = 31
```
Cette écriture est totalement équivalente à :

```py
if mois == 2:
    nbj = 28
else:
    if mois == 4 or mois == 6 or mois == 9 or mois == 11:
        nbj = 30
    else:
        nbj = 31
```

### Répétition

Python supporte deux types d'enchaînements répétitifs, également appelés « enchaînements itératifs » ou « boucles » : la boucle `while` et la boucle `for`.

#### La boucle while

Dans certains cas, il est nécessaire d'exécuter les mêmes instructions un nombre de fois variable selon les valeurs d'entrée de l'algorithme. Plus précisément, on répétera ces instructions tant qu'une condition est remplie. Dans ce cas, on utilisera une boucle `while`, qui est composée ainsi:

- première ligne :
    - le mot-clé `while`
    - l'expression booléenne représentant la condition,
    - le caractère `:`.
- enchaînement d'instructions à répéter tant que la condition est vraie
    - toutes avec un niveau d'indentation supérieur à la première ligne.

Ici encore, l'enchaînement d'instructions suivant le `while` peut comporter tous types d'enchaînements (séquentiel, conditionnel, répétitif). La première instruction précédée du même nombre d'espaces (ou moins) que la première ligne (la ligne du `while`) sera reconnue comme ne faisant pas partie de la boucle. Elle sera donc exécutée dès que la condition devient fausse.

Considérons par exemple l'algorithme suivant qui calcule le nombre de chiffres (en base 10) nécessaires à l'écriture d'un entier positif :

```py
"""
:entrée n:  int, AFFECTÉ précédemment
:pré-cond:  n > 0
:sortie c:  int, AFFICHÉ à l'écran
:post-cond: n s'écrit en base 10 avec c chiffres
"""
c = 1
while n > 10:
    c = c+1
    n = n//10
print(c)
```
On peut représenter l'enchaînement des instructions de cet algorithme par le diagramme ci-dessous :
![diagram](./pics/diagramWhileloop.png)

Il est intéressant de remarquer que, selon les valeurs en entrée, les instructions de la boucle peuvent être exécutée plusieurs fois, une seule fois, voire pas du tout (si n=7, par exemple, dans l'algorithme ci-dessus).

#### La boucle for

Certaines valeurs manipulées par un algorithme peuvent être vues comme « contenant » d'autres valeurs ; par exemple, une chaîne de caractères peut être vue comme une liste d'éléments plus simples que sont chacun de ses caractères.

Ces valeurs complexes sont qualifiées d'itérables en Python, c'est-à-dire que l'on peut itérer dessus. Autrement dit, on peut parcourir leurs éléments un par un, dans l'ordre, et appliquer le même ensemble d'actions sur chacun d'eux. Ceci s'effectue avec la boucle for, qui est composée ainsi :

- première ligne :
    - le mot-clé `for`
    - un nom de variable
    - le mot-clé `in`,
    - l'itérable sur lequel on veut boucler
    - le caractère `:`.
- enchaînement d'instructions à exécuter pour chaque élément de l'itérable
    - toutes avec un niveau d'indentation supérieur à la première ligne.

La première instruction précédée du même nombre d'espaces (ou moins) que la première ligne (la ligne du `for`) sera reconnue comme ne faisant pas partie de la boucle.

La variable nommée après le `for` prendra successivement pour valeur chacun des éléments de l'itérable; et pour chacun d'entre eux, les instructions de la boucle seront exécutées.

Considérons par exemple l'algorithme suivant qui retourne la chaîne de caractères « miroir » de la chaîne passée en entrée :

```py
"""
:entrée s:  str, SAISI au clavier
:pré-cond:  Ø
:sortie r:  str, AFFICHÉ à l'écran
:post-cond: r est la chaîne miroir de s
"""
s = input("s=")
r = ""
for c in s:
    r = c+r
print(r)
```
Par exemple, si l'utilisateur saisit la chaîne « épater », cet algorithme affichera « retapé ».

#### Boucles avec range

Il existe un cas particulier de boucle extrêmement fréquent : une boucle itérant sur des entiers successifs. Pour répondre à ce besoin, Python fournit la fonction `range`, qui fournit un itérable contenant une séquence d'entier. Plus précisément:

- `range(i)` itère sur l'intervalle[0,i[
- `range(i, j)` itère sur l'intervalle[i,j[

> Note à nouveau l'interprétation des bornes en Python. La borne inférieure est toujours inclue, la borne supérieure est toujours exclue. ‼️

Considérons par exemple l'algorithme suivant qui retourne la factorielle de l'entier n :

```py
"""
:entrée n:  int, AFFECTÉ précédemment
:pré-cond:  n ≥ 0
:sortie f:  int, AFFICHÉ au clavier
:post-cond: f = n! = 1×2×3×...×(n-1)×n
"""
f = 1
for i in range(2, n+1):
    f = f*i
print(f)
```
Notons que ce type de boucle `for` peut également s'écrire sous forme d'une boucle `while` :

```py
f = 1
i = 1          # 2   = valeur initiale du range
while i < n+1: # n+1 = valeur finale du range
    f = f*i
    i = i+1
print(f)
```
Le choix de l'une ou l'autre des écitures est une question de goût. La boucle`for` a l'avantage d'être plus concise, alors que la boucle `while` est plus explicite (notamment sur le fait qu'on ne rentre pas dans la boucle pour i = n+1). La boucle `while` est aussi plus générale : on peut faire varier i de différentes manières (par exemple, en le multipliant par deux à chaque itération), mais il est plus facile d'oublier l'initialisation de la variable (ligne 3 ci-dessus) ou sa modification en fin d'itération (ligne 6 ci-dessus).

![loop](./pics/loop.jpeg)

### Fonction

Une fonction est un enchaînement d'instruction auquel on donne un nom pour pouvoir le réutiliser plus tard. Toute fonction résoud un problème précis, et est donc accompagnée de la spécification de ce problème. À titre d'exemple, voici comment on peut ré-écrire l'algorithme ci-dessus, qui calcule la factorielle de n'importe quel entier strictement positif :

```py
def factorielle(n: int) -> int:
    f = 1
    for i in range(2, n+1):
        f = f*i
    return f
```
On constate que 
- la première ligne est composée ainsi :
    - le mot-clé `def`, qui annonce que nous définissons une nouvelle fonction
    - le nom de la fonction,
    - la liste des paramètres d'entrée, entre parenthèse et séparés par des virgules (le cas échéant),
        - chaque paramètre est décrit par son nom, suivi de deux points (`:`), suivi de son type de données, 
    - le type de la valeur de retour, précédé par `->`,
    - le caractère deux points (`:`).
- Toutes les lignes suivantes ont un niveau d'indentation supérieur à la première.
- La dernière ligne de l'algorithme comporte le mot-clé `return`, suivi de la valeur à donner au paramètre de sortie.

Lorsqu'une fonction possède plusieurs paramètres de sortie, leurs types (sur la première ligne) sont mis entre parenthèses et séparés par des virgules. Les valeurs retournées sont simplement séparées par des virgules :

```py
def encadre_racine_carrée(x: float) -> (int, int):
  inf = 0
  sup = 0
  # (...) séquence d'inscruction
  return inf, sup
```

#### Appel de fonction

Une fois que l'on a défini une fonction, cette dernière fait alors partie des « capacités » de l'ordinateur.

Pour indiquer à l'ordinateur qu'il doit appeler (ou utiliser) une fonction, on écrira une affectation dont :

- la partie gauche comportera autant de variables que la fonction comporte de paramètres de sorties ;
- la partie droite est constituée du nom de la fonction, suivi par la liste des valeurs des paramètres d'entrée, entre parenthèses et séparées, le cas échéant, par des virgules.

Par exemple :
```py
>>> bi, bs = encadre_racine_carrée(2.0)
>>> k = max(2*j, i-1)   # max() retourne la plus grande des valeurs passées en entrée
```

Dans les exemples ci-dessus, on voit que les valeurs passées aux paramètres d'entrée peuvent être des expressions complexes. On constate aussi que les variables recevant les valeurs des paramètres de sortie ne sont pas tenues d'avoir le même nom que ces paramètres (s'ils sont nommés) ; c'est l'ordre des variables qui détermine leur correspondance avec les paramètres de sortie.

Enfin, notons que, dans le cas particulier des fonctions n'ayant qu'un seul paramètre de sortie, l'appel à la fonction peut être utilisé directement dans une expression. Par exemple, au lieu d'écrire :
```py
>>> i = factorielle(5)
>>> j = i-1
```
on peut écrire directement :
```py
>>> j = factorielle(5)-1
```

#### Types particuliers de fonction

Ici quelques notions qui sont parfois utiles pour distinguer certains types particuliers de fonctions. C'est bon a savoir car dans de la doc tu peux tombée sur ces termes mais dans la pratique ça n'est pas si important de savoir tout ça. 🤗

**effet de bord**
Tout effet produit par une fonction en dehors des valeurs qu'elle retoure. Un exemple classique d'effet de bord est l'affichage d'information à l'écran. Dans le chapitre sur les [tableaux](#tableaux), on rencontrera un autre type d'effet de bord, lié aux paramètres d'entrée-sortie.

**fonction pure**
Toute fonction n'ayant aucun effet de bord, et dont les valeurs de retour dépendent exclusivement des valeurs passées en entrée. Des exemples de fonctions pures sont les fonctions mathématiques telles que sinus ou factorielle.

**procédure**
Une fonction ne retournant rien. Elle ne comporte donc pas d'instruction return, ou alors le return n'est suivi d'aucune valeur. Par définition, une telle fonction doit avoir des effets de bord (sans quoi elle n'aurait aucun effet, et donc aucune utilité). Un exemple de procédure est la fonction print, dont le seul effet est d'afficher les valeurs qui lui sont passées en entrée.

Puisqu'une procédure ne retourne aucune valeur, son appel se limitera au nom de la procédure suivi de ses paramètres, sans affectation :
```py
>>> print("bonjour le monde")
```
Notons qu'une fonction peut n'être ni une fonction pure, ni une procédure :

- la fonction input retourne une valeur (ce n'est donc pas une procédure) mais elle a aussi des effets de bord en affichant un message à l'écran et en sollicitant une action de l'utilisateur (ce n'est donc pas une fonction pure).
- La fonction randrange(start, stop) retourne un entier aléatoire compris entre start et stop. Puisqu'elle retourne une valeur, ce n'est pas une procédure. Mais cette valeur ne dépend pas uniquement des paramètres d'entrées : en appelant deux fois de suite randrange(1, 7), on peut obtenir deux résultats différents. Ce n'est donc pas non plus une fonction pure.

### Variables de fonction

On a présenté au un peu plus tôt la notion de variable. Il s'agit ici de décrire les différentes catégories de variables utilisées dans une fonction. Elles sont au nombre de trois :
- les variables correspondant aux paramètres d'entrée ;
- les variables correspondant aux paramètres de sortie ;
- les variables intermédiaires.

Les variables correspondant aux paramètres d'entrée ont une particularité : elles ont déjà une valeur au début de la fonction. Elles n'ont donc pas besoin d'être affectées, et on évitera en général de changer leur valeur.

Les variables correspondant aux paramètres de sortie, quant à elles, doivent absolument être affectées dans la fonction, puisque c'est le rôle de cette dernière de déterminer leur valeur (rappelons que les paramètres de sortie décrivent la solution au problème que l'on cherche à résoudre). Ces valeurs sont transmises à l'appelant par l'instruction `return` à la fin de la fonction.

Les variables intermédiaires, enfin, sont toutes les autres variables qui peuvent être nécessaires au calcul des paramètres de sortie. Par définition, elles n'ont pas de valeur initialement (elle doivent donc être affectées avant d'être utilisées), et leur valeur est « oubliée » à la fin de la fonction (puisqu'elles ne sont pas données à l'instructin `return`).

Si l'on voit la fonction comme une boîte noire dont le rôle est d'effectuer une opération précise, alors :
- les paramètres d'entrée contiennent les valeurs passées à la boîte noire ;
- les paramètres de sortie contiennent les valeurs retournées par la boîte noire ;
- les variables intermédiaires sont invisibles en dehors de la boîte noire.

Imaginons que le rôle de notre boîte noire soit de déterminer quelles sont la plus petite et la plus grande valeur d'une liste de valeurs, ainsi que la moyenne des éléments de la liste. La liste de valeur est notre paramètre d'entrée, le minimum, le maximum et la moyenne sont les paramètres de sortie, et on imagine aisément que d'autres variables temporaires sont utilisées à l'intérieur de la fonction pour effectuer les calculs intermédiaires (par exemple, la somme des valeurs et le nombre de valeurs, nécessaires au calcul de la moyenne).

#### Portée des variables

Les variables utilisées dans une fonction sont propres à cette fonction. Elles ne sont ni visibles, ni utilisables depuis d'autres fonctions, même si ces dernière définissent une variable ayant le même nom. On dit que la portée de la variable est limitée à la fonction qui la définit.

Exemple 😌

```py
def nb_chiffres(n: int) -> int:
    """
    :pré-cond:  n > 0
    :post-cond: retourne le nombre de chiffres nécessaires
                pour écrire n en base 10
    """
    c = 1
    while n > 10:
        c = c+1
        n = n//10
    return c

def total_chiffres_fact(n: int) -> int:
    """
    :pré-cond:  n > 0
    :post-cond: retournr le nombre total de chiffres nécessaires pour
                écrire les factorielles de tous les entiers entre 1 et n
    """
    f = 1
    c = 0
    for i in range(1, n+1):
        f = f*i
        c = c+nb_chiffres(f)
    return c
```
La fonction `total_chiffres_fact` appelle la fonction `nb_chiffres`. On remarquera que ces deux fonctions utilisent les mêmes noms de variable (n, c). Cependant, il n'y a aucune interaction entre la valeur de n (respectivement de c) dans `nb_chiffres` et la valeur de n (respectivement de c) dans `total_chiffres_fact`.

Il faut imaginer que, lorsque l'ordinateur exécute `total_chiffres_fact`, il stocke les valeurs de n, c et f dans un emplacement E1 de sa mémoire, qui est dédié aux variables définies dans cette fonction. Lorsqu'à la ligne 13, on lui demande d'exécuter la fonction `nb_chiffres`, il laisse de coté l'emplacement E1 et commence à travailler sur un emplacement E2, dédié aux variables de `nb_chiffres`, dans lequel il va stocker les valeurs de n et c de `nb_chiffres`. Lorsque cette dernière se termine, l'emplacement E2 est supprimé, et l'ordinateur travaille à nouveau sur l'emplacement E1 pour terminer l'exécution de `total_chiffres_fact`, ou n et c ont gardé les valeurs qu'elles avaient juste avant l'appel à `nb_chiffres`.

![diagramStockageVariables](./pics/diagramStockageVariables.png)
> Les variables de chaque fonction existent à des endroits différents de la mémoire, même lorsqu'elles ont le même nom.

Tu peux faire la simulation sur [Pythontutor](https://pythontutor.com) pour mieux comprendre comment sont gérés les espaces mémoires. (C'est loin d'être urgent mais tu m'a l'air bien curieuse alors je te laisse ca là 😉)

Il est cependant important de détailler ce qui se passe aux moments du passage de E1 à E2, et du retour de E2 à E1 :

- au moment de passer de la fonction appelante (E1) à la fonction appelée (E2), les expressions passées aux paramètres d'entrée (entre les parenthèses) sont calculées avec les variables de E1, et leurs valeurs sont affectées aux variables correspondantes dans E2 ;
- au moment de revenir de la fonction appelée à la fonction appelante, les valeurs des paramètres de sortie sont affectées aux variables correspondantes dans E1, ou substituées à l'appel de fonction si celui-ci est utilisé directement dans une expression.

Considérons l'exemple ci-dessus, où on aurait appelé la fonction `total_chiffres_fact` avec n=5. Au premier passage à la ligne 13, les variables de `total_chiffres_fact` ont les valeurs suivantes : n=5, c=0, i=1 et f=1. À ce moment, l'ordinateur calcule les valeurs à passer aux paramètres d'entrée de `nb_chiffres`, en l'occurrence un seul paramètre, dont la valeur est 1 (valeur de f). L'emplacement mémoire qui contiendra les variables de `nb_chiffres` est donc initialisé avec n=1. À la fin de l'exécution de `nb_chiffres`, ses variables ont pour valeur n=1 et c=1 (cf figure 1). Comme `nb_chiffres` est utilisée directement dans une expression, la valeur du paramètre de sortie c est substituée à l'appel de fonction, donc la ligne 13 de `total_chiffres_fact` revient ici à calculer :

```py
c = c+1
```
Dans l'emplacement mémoire de `total_chiffres_fact`, donc avec c=0. Notons aussi que la valeur de n dans `total_chiffres_fact` est toujours 5, et n'a pas été influencée par le fait que `nb_chiffres` utilisait une variable du même nom avec une valeur différente.

L'apparente complexité de ce processus est en fait une simplification : elle permet au programmeur d'une fonction f de ne pas se soucier des noms de variables utilisés dans les autres fonctions (celles appelées par f comme celles qui appellent f). Il favorise donc la modularité du code.

Encore une fois toute cette partie est trés technique et bonne a savoir mais pour le début ne te prend pas trop la tete dessus on aura certainement l'occasion d'aborder le sujet plus tard ! 

## Tableaux
[sommaire](#sommaire)

> Le type tableau présenté ici (numpy.array) n'est pas le plus courant en Python, qui préfère l'utilisation des listes. Il présente cependant un certain nombre d'intérêts, dont celui d'être plus proche des types tableaux disponibles dans d'autres langages de programmation mais aussi que pour les calcules de **Matrices** pour l'IA on préfère des tableaux ordonées aux bête liste de Python. Une simple recherche [ici par exemple](https://www.w3schools.com/python/python_arrays.asp) suffira a ce que tu comprenne que c'est hyper simple. 🤗 

Un tableau est une liste ordonnée de n valeurs du même type. On appelle n la taille du tableau, et les valeurs qu'ils contient sont ses éléments. Chaque élément est repéré dans le tableau par son indice, un nombre entier compris entre 0 et n-1 (inclus).

**Pré-requis**
Afin d'utiliser des tableaux en Python, il est nécessaire :
- installer Numpy,
- inclure la ligne suivante dans tous les programmes utilisant les tableaux :
```py
from numpy import *
```
![numpy](./pics/numpy.jpeg)

### Déclarer un tableau

Un tableau peut-être déclaré de plusieurs manière :
```py
>> from numpy import *
>>> array([5,3,2,1,1]) # crée un tableau à partir de ses éléments
array([5, 3, 2, 1, 1])
>>> zeros(3, float)    # crée un tableau rempli de 0
array([ 0., 0., 0.])
>>> empty(4, int)      # crée un tableau non initialisé
array([       0,  8826784, 31983376,        0])
```
On passe à `array` la liste des éléments du tableau à créer ; on note que les valeurs doivent être encadrées par des crochets `[...]` en plus des parenthèses.

On passe à `zeros` et `empty` la taille du tableau et le type de ses éléments. Notons cependant que le nom `empty` prête à confusion : il ne crée pas un tableau vide (le tableau contient des éléments), mais il n'initialise pas ses éléments, donc ceux-ci ont une valeur aléatoire. Lorsqu'on utilise `empty`, il est donc impératif d'initialiser ensuite chaque élément du tableau, sans quoi la suite de l'algorithme risque de donner des résultats aléatoires.

### Opérations sur les tableaux

Les opérations disponibles sur les tableaux sont très similaires aux opérations disponibles sur les chaînes de caractères :
```py
>>> a = array([5,3,2,1,1])
>>> len(a)  # longueur
5
>>> a.size  # autre manière d'obtenir la longueur d'un tableau
5
>>> a[0]    # premier élément
5
>>> a[:3]   # sous-tableau correspondant aux 3 premiers éléments
array([5, 3, 2])
>>> a[3:]   # sous-tableau correspondant aux éléments à partir du 4ème
array([1, 1])
```
On peut par ailleurs modifier un élément d'un tableau (identifié par son indice) en utilisant une affectation :
```py
>>> a = array([5,3,2,1,1])
>>> a[1] = 9  # modification du 2ème élément du tableau
>>> a
array([5, 9, 2, 1, 1])
```

### Paramètres de type tableau

Pour déclarer qu'un paramètre d'entrée ou de sortie est un tableau, on indiquera le type d'élément du tableau entre crochet :
```py
def double(tab: [float]) -> [float]:
  """
  :post-cond: retourne un tableau dont les éléments valent
              le double de ceux de tab
  """
  ret = empty(len(tab))
  for i in range(len(tab)):
      ret[i] = 2*tab[i]
  return ret
```

### Tableaux et mutabilité

Toutes les opérations portant sur les types de données vus précédemment (entier, flottant, booléen et chaîne) produisent une nouvelle valeur à partir des valeurs des opérandes. Chaque affectation d'une variable remplace donc la valeur de cette variable par une autre valeur.

Une conséquence est que la modification d'une variable ne peut pas avoir d'impact sur une autre variable. Par exemple :
```py
>>> a = 42
>>> b = a   # b prend la même valeur que a, c.à.d. 42
>>> a = a+1 # a+1 produit la valeur 43, qui remplace 42 dans la variable a
>>> a
43
>>> b       # b, en revanche, contient toujours 42
42
```

Avec les tableaux, l'affectation d'un élément modifie l'état du tableau sans remplacer ce dernier par un autre tableau. On dit que les tableaux sont des objets mutables (c'est à dire modifiable; le terme anglais est également mutable). Il en résulte que, si deux variables font référence au même tableau, une modification sur l'une des variables sera répercutée sur l'autre :
```py
>>> a = array([5,3,2,1,1])
>>> b = a  # a et b font maintenant référence au MÊME tableau
>>> b
array([5, 3, 2, 1, 1])
>>> a[1] = 9
>>> a      # l'état du tableau a été modifié...
array([5, 9, 2, 1, 1])
>>> b      # ... ce qui se voit aussi sur b, puisqu'il s'agit du même tableau
array([5, 9, 2, 1, 1])
```

De plus, les sous-tableaux produits par les opérations a[:i] et a[i:] partagent également l'état du tableau qui a servi à les produire. Ce ne sont pas des copies partielles, mais des vues restreintes du tableau global. Ainsi, si on modifie un élément du sous-tableau, le tableau global est également modifié (et inversement) :
```py
>>> a = array([5,3,2,1,1])
>>> b = a[3:]
>>> b
array([1, 1])
>>> b[1] = 7
>>> b
array([1, 7])
>>> a
array([5, 3, 2, 1, 7])  # la modification de b est répercutée sur a
>>> a[3] = 8
>>> a
array([5, 3, 2, 8, 7])
>>> b
array([8, 7])           # la modification de a est répercutée sur b
```
### Mutabilité égalité et identité 

on a vu précédemment que l'opérateur `==` permet de vérifier l'égalité deux deux éléments.

Cela dit, cet opérateur a un comportement pour le moins étonnant lorsqu'on l'utilise avec des tableaux. Considérez le fragment de code suivant.
```py
taba = array([1,2,3])
tabb = array([1,2,3])
tabc = array([1,2,4])
tabd = array([1,2,4,5])

print(taba == tabb)
print(taba == tabc)
print(taba == tabd)
```
L'opérateur `==` sur des tableaux numpy va retourner :
- False si les tableaux sont de taille différentes
- Un tableau de booléens comparant les éléments un a un

Par conséquent, comme le tableau résultat n'a pas de valeur logique unique, on ne peut pas écrire :
```py
# ⚠ ce code ne fonctionne pas
if taba == tabb:
   print("les tableaux sont égaux")
```
Comme nous venons de le voir, l'opérateur `==` permet de vérifier, élément par élément, l'égalité de deux tableaux. Mais dans certains cas, on peut vouloir vérifier que deux noms de variables représentent le même objet "tableau". Pour cela, on va utiliser l'opérateur `is`, qui permet de vérifier l'identité des objets (rassurez-vous, vous en apprendrez plus long sur les objets au prochain semestre). Considérez l'exemple suivant :
```py
 >>> taba = array([1,2,3])
 >>> tabb = array([1,2,3])
 >>> tabc = taba
 >>> print(taba == tabb)
 [ True  True  True]
 >>> print(taba == tabc)
 [ True  True  True]
 >>> print(taba is tabb)
 False
 >>> print(taba is tabc)
 True
>>> taba[2] = 8
>>> print(taba)
[1 2 8]
>>> print(tabb)
[1 2 3]
>>> print(tabc)
[1 2 8]
```
Vous constatez bien que `taba` et `tabb` sont égaux car toutes leurs valeurs sont égales une à une. En revanche, `taba` et `tabb` sont deux objets différents (si l'on modifie `taba`, `tabb` ne sera pas modifié) tandis que `taba` et `tabc` sont deux noms différents pour le même objet (si l'on modifie `taba`, `tabc` sera modifié). 🥵

### Tableaux de chaînes de caractères

Un peu plus légère cette partie promis !😇
Les tableaux de chaînes de caractères nécessitent quelques précautions, car la taille maximale de leurs éléments doit être connue à la création du tableau.

Considérons les instructions suivantes :
```py
>>> a = array(["un", "deux"])
>>> a[0] = "autre chose"
>>> a
>>> a[0]
'autr'
```
Lorsqu'on crée un tableau de chaînes avec `array`, la taille maximale des éléments est fixée à la longueur du plus grand élément fourni. Il faut être très attentif à cela, car on voit qu'ensuite, les valeurs affectées au tableau sont silencieusement tronquées à cette taille.

Par ailleurs, si l'on souhaite créer un tableau de chaîne de caractères avec `zeros` ou `empty` et que l'on passe str comme type de données, la taille maximale des éléments sera fixée à un caractère.

Si l'on souhaite un tableau de chaînes de caractères de longueur supérieure, il faudra passer en guise de type un chaîne de caractères formée du préfixe `<U`suivi de la longueur souhaitée. Par exemple, la ligne suivante :
```py
a = zeros(10, "<U1000")
```

créera un tableau de 10 chaînes de caractères de longueur maximale 1000.

Notons enfin que, pour les chaînes de caractères, `zeros` initialise les éléments du tableau par une chaîne vide (`''`).

## Récursivité
[sommaire](#sommaire)

Ça fait beaucoup déjà mais on va finir sur ce petit point qui est trés important en programation ! Littéralement cette methode peut te faire économiser beaucoup de ligne de code et donc de temps et de bug..

Alors !

Une fonction récursive est une fonction qui s'appelle elle-même. Chaque appel à la fonction est indépendant des autres, avec ses propres variables. Tout est dit pourtant c'est pas si intuitif a mettre en place tu verra.

![recurtion](./pics/recurtion.jpeg)

Une récursion a toujours la forme suivante :
```py
if (cas simple):
    (solution immédiate)
else:
    (solution récursive,
    impliquant un cas plus simple que le problème original)
```

L'exemple le plus classique d'emploi de la récursivité est l'écriture de la fonction factorielle. Pour rappel, la factorielle d'un nombre n est définie comme n fois la factorielle du nombre n-1, et la factorielle de 1 est 1.
```py
    """
    :pré-cond:  n > 0
    :post-cond: retourne n! = 1×2×3×...×(n-1)×n
    """
    if n == 1:
       f = 1
    else:
       f = factorielle(n-1)*n
    print(f"--- factorielle({n}) = {f}")
    return f

>>> print(factorielle(6))
--- factorielle(1) = 1
--- factorielle(2) = 2
--- factorielle(3) = 6
--- factorielle(4) = 24
--- factorielle(5) = 120
--- factorielle(6) = 720
720
```

[Haut de page](#sommaire)
