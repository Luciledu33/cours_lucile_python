# Principe du moindre privilège

Le **principe du moindre priviège** ou ***least privilege principle*** est un principe en informatique qui désigne le fait que chaque entité, chaque tâche, ne doit pouvoir accéder qu'à une quantité limitée de données : le strict nécessaire pour mener à bien la tâche en question.

![YOU GET DENIED](./pics/you_get_denied.jpg)

---

Par exemple, il est normalement inutile pour votre navigateur Web d'accéder à vos photos de vacances : ça ne lui est pas nécessaire pour réaliser sa tâche première qui est de vous permettre d'accéder à des sites Web.

Côté serveur : normalement un serveur Web n'installe pas de nouveaux paquets sur votre système. Il n'est là que pour servir des pages HTML, JS, CSS, etc.

**Ainsi on essaie de respecter aussi bien sur les machines clientes que sur les serveurs le principe du moindre privilège.**

> Vous trouverez plein de documentation à ce sujet sur Internet, comme [une page Wikipedia](https://fr.wikipedia.org/wiki/Principe_de_moindre_privil%C3%A8ge) bien sûr.

**C'est donc un principe extrêmement élémentaire qui permet de se prémunir d'une écrasante majorité d'attaque.**

> C'est la base. La base de la base. La base des fondations des fondements. De la sécu.

## Sommaire

- [Principe du moindre privilège](#principe-du-moindre-privilège)
  - [Sommaire](#sommaire)
  - [1. Gestion de permissions et d'utilisateurs](#1-gestion-de-permissions-et-dutilisateurs)
    - [A. Les choses à éviter](#a-les-choses-à-éviter)
    - [B. Les choses à faire](#b-les-choses-à-faire)
    - [C. Quelles permissions sur quel fichier](#c-quelles-permissions-sur-quel-fichier)
  - [2. Utilisateurs applicatifs](#2-utilisateurs-applicatifs)
  - [3. Firewall](#3-firewall)
  - [4. Spécial dédicace à SELinux](#4-spécial-dédicace-à-selinux)

## 1. Gestion de permissions et d'utilisateurs

> Il est essentiel d'avoir assimilé [le cours-notion sur les permissions POSIX](../../notions/permissions/README.md) (les droits `rwx`) avant de continuer.

Toutes les actions effectuées sur un système sont effectuées sous l'identité de quelqu'un.  
Les "actions" étant des commandes, des binaires, des exécutables que l'on lance, on peut aussi dire que tous les processus lancés sur la machine tournent sous l'identité d'un utilisateur donné.

Ainsi, chaque processus pourra ou ne pourra pas faire certaines actions en fonction de ce que l'utilisateur qui a lancé ledit le processus a ou n'a pas le droit de faire.

Ainsi, si un utilisateur `toto` ne possède **AUCUN** fichier sur le système, alors son champs d'action sera très limité.

---

Pour rappel :

- tous les programmes sont des fichiers
- tous les périphériques branchés à la machine sont des fichiers
- les librairies, les données des applications, la conf du système, quasiment TOUT est fichier
- donc si vous gérez bien vos permissions vous gérez la sécurité de TOUT quasiment

Donc on gère les users et les permissions de façon clean svp !

### A. Les choses à éviter

➜ **Le `chmod 777 <FICHIER>` est en parfaite opposition au principe du moindre privilège.**

En effet, la commande aura pour effet de donner tous les droits à tous les utilisateurs de la machine sur ce fichier.

> N'importe quel hacker qui se pointe peut alors, par exemple, écrire du code dans ce fichier et l'exécuter, sans pb, en étant l'utilisateur le moins privilégié de la machine.

---

➜ **Une mauvaise gestion de `root`**

`root` ne doit pas être considéré par les admins comme un utilisateur mais plutôt comme une identité, que l'on peut temporairement obtenir, afin d'exécuter des actions privilégiées sur la machine.

Autrement dit : n'utilisez pas directement `root` et préférez systématiquement utiliser la commande `sudo`, de façon ponctuelle, pour chaque action privilégiée que vous souhaitez effectuer.

Les raisons à ça sont multiples. Utiliser `root` directement a les conséquences suivantes :

- **tous les fichiers et dossiers que vous créerez appartiendront à `root`**
  - cela peut poser des problèmes de sécurité plus tard (laisser traîner des fichiers appartenant à `root`)
  - cela peut empêcher certains programmes de fonctionner correctement
    - car un fichier appartenant à `root` n'est pas forcément lisible par les autres users
    - car un fichier appartenant à `root` est considéré comme sensible, ils pourront refuser de l'utiliser
- **vous amène à une mauvaise connaissance du système**
  - utilisez `sudo` et JAMAIS `root` permet d'apprendre de façon empirique les actions qui ont besoin des droits `root` et celles qui n'en ont pas besoin
    - savoir cela amène à une bonne compréhension du fonctionnement du système
- **vous fait prendre de mauvaises habitudes**
  - vous laisserez traîner des fichiers appartenant à `root` partout
  - les actions qui nécessitent les droits `root` sont des actions qui peuvent grandement impacter la machine
    - vous ferez des actions impactantes sans même vous en apercevoir
    - vous n'aurez pas de deuxième chance en cas d'une commande malencontreuse

---

➜ **Sous-estimer l'impact des droits `r`, `w` et `x`**

> Rien n'est exhaustif ici, c'est pour vous donner une idée.

`r` permet de lire. Il est extrêment sensible de laisser ce droit sur des fichiers sensibles :

- fichiers de clés
- fichiers de mot de passe
- fichiers de conf d'applications sensibles

`w` permet d'écrire. Il permet de modificer le contenu d'un fichier. Par extension il peut permettre de :

- corrompre l'accès au système
  - changer les clés/mots de passe/utilisateurs
- corrompre les données des applications
- modifier le comportement des applications
  - en changeant les fichiers de conf

**`x` permet d'exécuter. C'est le droit le plus sensible des 3.** En effet, si on a le droit `x` sur un fichier,  on peut exécuter du code sur la machine.  
Si on a `w` en plus, on peut modifier le fichier puis l'exécuter c'est à dire choisir le code qu'on exécute.

> Un hacker qui a un fichier à disposition avec `w` et `x` dessus, il a quasiment gagné la partie : il choisit arbitrairement d'exécuter du code sur la machine.

### B. Les choses à faire

➜ **Gérer les permissions de TOUS LES FICHIERS DU SYSTEME**

Hé oui. Tous. Sans exception. But don't worry :

- tous les fichiers téléchargés *via* des paquets sont censés déjà respecter ce principe du moindre privilège
  - en particulier sur des distribs orientées serveur comme Rocky ou CentOS
  - pour rappel, l'OS lui-même est constitué de paquets, donc ça comprend aussi les fichiers présents à l'install
- vous devez donc gérer les permissions des fichiers que vous créez vous-mêmes
  - c'est tout de suite + tolérable comme quantité de fichiers
  - **tu crées un fichier, tu gères ses permissions. No questions about it. Tu le fais. C'est tout.**
  - **TU LE FAIS J'AI DIT**

![Look a thiiiis](./pics/look_at_this.jpg)

➜ **Utiliser des utilisateurs applicatifs**

C'est l'objet de [la prochaine section :)](#2-utilisateurs-applicatifs).

### C. Quelles permissions sur quel fichier

Pour savoir quelles permissions poser sur tel ou tel fichier, va falloir utiliser votre tête. Y'a pas de règle ultime.

**Faut vous demander quel utilisateur ira lire, écrire ou exécuter vos fichiers. Et appliquer les permissions en accord.**

Par exemple :

- la conf
  - ça doit être lisible par le programme qui va l'utiliser
  - ça doit être écrivable par un utilisateur admin (ou `root`, qu'on utilisera avec `sudo`)
  - c'est JAMAIS exécutable
- les logs
  - ça doit être lisible par un utilisateur admin
  - ça doit être écrivable par le programme qui va l'utiliser
  - c'est JAMAIS exécutable
- les données d'application
  - genre les machins dans `/run` et `/var`
  - c'est lisibles/écrivables par l'application concernée par telle ou telle donnée
  - personne d'autre peut lire/écrire
  - c'est JAMAIS exécutable

T'façon, quasiment rien n'est exécutable, à part les commandes et les scripts du système. Et encore è_é

> C'est le premier réflexe d'un hacker lorsqu'il chope un accès à une machine : chercher les fichiers exécutables. Avec `root` de préférence. [Par "chercher" on entend évidemment "trouver instantanément" avec par exemple : `find / -user root -type f -perm +111`](https://stackoverflow.com/questions/4458120/search-for-executable-files-using-find-command).

## 2. Utilisateurs applicatifs

Alors oui oui c'est bien beau tout ce qu'on a raconté avant, mais  comment on limite le champ d'action d'une application en cours de fonctionnement ?

En utilisant des ***utilisateurs applicatifs***. Il faut en user et abuser.

**Le principe est simple, les *utilisateurs applicatifs*** :

- sont de simples utilisateurs du système
  - comme l'utilisateur `toto` que vous utilisez le matin pour vous connecter à votre machine
- ne peuvent pas se connecter à la machine
  - suivant l'OS, la mise en oeuvre de ce trait change
    - sous GNU/Linux, on définit son shell comme étant `/usr/sbin/nologin` par exemple
  - mais le concept reste le même : si vous essayez de vous log avec ce user, ça ne fonctionnera pas
- peuvent donc être perçus uniquement comme des "identités", et pas comme de vrais users
  - on peut pas se connecter avec
  - MAIS on peut exécuter des commandes sous leur identité
    - par exemple avec `sudo -u <USER_APPLICATIF> <COMMANDE>`
    - ou encore en créant un [service](../../notions/serveur/README.md) qui sera exécuté sous l'identité de cet utilisateur

**Ainsi, un utilisateur applicatif a pour seule vocation d'être l'utilisateur qui lance telle ou telle application. Et uniquement cette application.**

> Il sera l'utilisateur qui apparaîtra comme celui qui a lancé le processus dans un Gestionnaire des tâches ou la commande `ps`.

Il aura donc des droits très restreints sur les fichiers de la machine : il ne pourra utiliser que ceux qui concernent l'application qu'il lance.

---

Typiquement :

- vous lancez un serveur Apache ?
  - il est lancé sous l'identité de `apache`, un utilisateur applicatif
- vous lancez une base de données MySQL ou MariaDB ?
  - il est lancé sous l'identité de `mysql`, un utilisateur applicatif
- etc etc

> On pourrait même imaginer lancer nos propres applications clientes sous des users applicatifs, ça serait franchement pas déconnants quand on voit à quel point certaines sont éclatées. Un navigateur Web, c'est notre fenêtre sur le monde, on pourrait utiliser un utilisateur dédié pour lui, ça ferait pas de mal.

## 3. Firewall

> Il existe [une section dédiée à la notion de Firewall dans le cours-notion sur les ports](../../notions/port/README.md).

Le firewall est une application présente sur toutes les machines. Elle permet de filtrer le trafic qui entre et le trafic qui sort d'une machine donné.

**Le firewall DOIT être configuré selon le principe du moindre privilège** :

- tout le trafic est bloqué par défaut (whitelist)
- on autorise que le strict nécessaire

**Par exemple, pour un serveur Web :**

- on autorise tout trafic entrant sur le port 80
  - pour servir des sites en HTTP (pour les clients)
  - typiquement on redirige vers le port 443 pour forcer HTTPS
- on autorise tout trafic entrant sur le port 443
  - pour servir des sites en HTTPS (pour les clients)
- on autorise tout trafic entrant sur le port 22
  - pour administrer le serveur en SSH (pour les admins)

**Et c'est tout.** Ceci implique :

- tout autre trafic entrant est bloqué
- tout trafic sortant est bloqué
  - la machine ne peut initier aucune connexion vers l'extérieur
  - strictement aucune hihi :3

## 4. Spécial dédicace à SELinux

SELinux, c'est le truc que je vous ai fait désactiver direct dans la machine Rocky.

C'est une application très puissante qui permet d'augmenter le niveau de sécurité du système. Le concept est simple : toute action sur le système est bloquée par défaut. C'est un système géant de whitelist.

Je vais pas m'étendre sur lui ici, ça dépasse le cadre du cours, mais simplement pour le mentionner, et préciser qu'il peut être très utile pour mettre en place ce principe du moindre privilège de façon encore plus drastique.

![Everytime you disable SELinux](./pics/everytime_selinux.jpg)