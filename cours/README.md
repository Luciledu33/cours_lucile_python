# Cours

## ➜ [Cours](.cours/README.md)

- [Intro aux "bonnes pratiques" de l'informatique.](./cours/intro/README.md)

- [Principe du moindre privilège](./cours/least_privilege_principle/README.md)

- [Introduction a Python](./cours/intro_python/README.md)

## ➜ [Notions](./notions/README.md)

Mini-cours sur des notions précises.
- [Git](./notions/git/README.md)
