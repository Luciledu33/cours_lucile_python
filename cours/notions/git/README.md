# Git

- [Git](#git)
- [I. Le principe](#i-le-principe)
- [II. Le flooooooooooooow](#ii-le-flooooooooooooow)
- [III. git status](#iii-git-status)

**Git est un outil de versionning.** C'est à dire que ça permet de gérer des dossiers mais de façon un peu plus évoluée que de simples dossiers.

> Git a été développé par Linus Torvalds. Uè uè le même gars qui a dév le noyau Linux (il est plus tout seul à maintenir ces outils maintenant hein).

![Git wasn't made to make you smile](./pics/i_did_not.jpg)

Git c'est le feu, la plupart des codes de la planète sont hébergés dans des dépôts git (que ce soit le code de Linux, de Netflix, de Google, de WoW, de LoL, de tout en fait). Donc vous vous doutez que c'est pas de la marde inutile. Nan ?

**Git est juste excessivement pratique et puissant quand on sait s'en servir.**

Ce doc n'a pas DU TOUT pour but de vous apprendre à maîtriser git, mais seulement comprendre de quoi il s'agit, et en avoir une utilisation au moins rudimentaire.

**Le strict minimum pour capter ce que vous faites + me rendre les TPs.**

# I. Le principe

**Un dépôt git, c'est un dossier.**

Voilà fin du cours. è_é

SVP imprimez ça dans votre tête : un dépôt git, c'est un dossier.

C'est pas une image, une métaphore ou une analogie : **UN DEPOT GIT C'EST JUSTE UN DOSSIER**.

Donc on arrête de paniquer SVP, on se prend en main, et normalement, vous savez déjà utiliser un dossier non ? Clic-droit > Nouveau Dossier, ça va, vous gérez ?

**Le délire de git c'est d'avoir un dossier sur un serveur central (c'est le *dépôt git*), et tout le monde en récupère une copie.**  
Ainsi, quand on veut modifier le dossier partagé sur le serveur central (= le *dépôt git*), on fait d'abord la modif sur notre PC, sur la copie donc, puis on envoie notre modif à distance.

**ET VOUS POUVEZ M'EXPLIQUER CE QU'IL Y A DE COMPLIQUE LA DEDANS SVP ?**

On parle juste de faire des copier-collers entre un dossier sur votre PC, et un dossier sur un serveur.

DONC, on arrête de paniquer, vous savez déjà très bien vous servir des dépôts git si vous alignez un peu vos neurones.

# II. Le flooooooooooooow

Le workflow git de base, c'est à dire, comment on s'en sert usuellement :

- au début, on "clone" une fois le dépôt qui se trouve sur le serveur
  - c'est juste le fait de récup le dossier en local
- après, on a une copie du dossier sur notre PC
  - donc on peut faire des modifs dedans
  - de toute évidence, une modif sur notre PC n'est PAS instantanément répercutée sur le dossier du serveur central (le dépôt)
- une fois qu'on a fait des modifs, on indique LOCALEMENT à git qu'on ajouté ou modifié des fichiers de notre côté
  - faut tout lui dire à ce p'tit
- puis on envoie les modifs sur le serveur central

Avec des commandes ça donne :

```bash
# On récupère sur notre PC, en local, UNE SEULE FOIS GGNNGNNNNN le dépôt git
$ git clone https://URL/super_depot
# Ca crée un nouveau dossier, qui porte par défaut le nom du dépôt qu'on a cloné

# On fait des modifs
$ cd super_depot
# Je fais une modif de merde avec une commande vitefé, mais vous pouvez ajouter ce que vous voulez, du bloc note, du code, du .mp3, balec
# BALEC C JUST UN DOSIER TTE FASSON
$ echo "super modif" > super_fichier

# On dit à git qu'on a ajouté/modifié un fichier
$ git add super_fichier

# Si on est fainéants on peut lui dire à la place "hé gros, j'ai potentiellement tout modifié"
# Mais c'est assez mal vu, vaut mieux préciser explicitement ce qu'on a modif avec git add
# Je vous file la commande quand même, because i'm evil
$ git add . # DON'T DO THIS AT HOME KIDS

# Ensuite, on donne un nom à nos modifications
$ git commit -m "j'ai ajouté un super fichier au super dépôt :o"

# Puis, on envoie les modifs qu'on a fait localement à distance
$ git push
```

Résumé :

- `git clone` une seule fois : ça sert à récup le dossier
- ensuite c'est un cycle :
  - modifications/ajout/suppression de fichiers
  - `git add`
  - `git commit`
  - `git push`
  - and repeat

![Forgot to push](./pics/forget_push.jpg)

# III. git status

Aussi je vous recommande de SPAMMER la commande `git status` qui vous indiquera en permanence :

- si votre dossier est synchro avec le dépôt git du serveur central, genre s'il est à jour
- s'il y a des fichiers que vous avez modifié par rapport au dossier central
- s'il y a des fichiers que vous avez ajouté par rapport au dossier central
- s'il y a des fichiers que vous avez supprimé par rapport au dossier central
- s'il y a des fichiers que vous avez modifié/ajouté/supprimé mais n'avez pas encore *commit* (c'est le fait de donner un nom à nos modifs)
- s'il y a des fichiers que vous avez modifié/ajouté/supprimé que vous avez *commit* mais que vous avez pas encore *push*

Fin la commande `git status` va vous carry quoi. Il faut juste taper et lire ce qu'elle retourne.