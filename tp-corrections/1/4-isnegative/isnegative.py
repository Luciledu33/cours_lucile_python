# TP-1 isnegative
# Alexandre Pajak

# Instructions:
# Write a function that prints 'T' (true) on a single line if the int passed as parameter is negative, 
# otherwise it prints 'F' (false).

# Expected function:
# def IsNegative(nb):

# Usage:
# Here is a possible program to test your function :
# def main() :
#   IsNegative(1)
#   IsNegative(0)
#   IsNegative(-1)

# And its output :
# % python3 isnegative.py
# F
# F
# T
# %


#### code: ####

def IsNegative(nb: int) : # fonction with argument (displayeing "int" when is called")
    if isinstance(nb, int) : # check if nb variable is an integer
        if nb < 0 : # check if is less than 0
            print("T")
        else :      #  otherwise is upper or equal so print "F"
            print("F")
    else :                  # otherwise is not an integer 
        print("error, nb is not an integer.")

def main():
    IsNegative(1)
    IsNegative(0)
    IsNegative(-1)
    IsNegative("a")

main()