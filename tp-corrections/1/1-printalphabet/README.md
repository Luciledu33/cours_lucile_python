# TP1

## PrintAlphabet

### Objectif:
Ici le but est d'afficher les lettres de l'alphabet alignées et sans séparation.

### Methode:
Pour afficher du texte dans le terminal on utilise la fonction print() qui prend en argument des Strings et les retourne dans le terminal.

> Les Strings sont notées "Str" en Python.

## code:
[suggestion de code](printalphabet.py)