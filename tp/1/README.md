# Intro a python.

Ce premier TP a pour objectif de te familiariser avec les bases du langague Python. 
Tu vas apprendre a créer et manipuler des fonction (def) en Python afin de résoudres quelques problèmes basiques.

## sommaire

- [PrintAlphabet](#printalphabet)
- [PrintReversAlphabet](#printreversalphabet)
- [PrintDigits](#printdigits)
- [IsNegative](#isnegative)
- [Printcomb](#printcomb)
- [Printcomb2](#printcomb2)
- [PrintCombN](#printcombn)


## PrintAlphabet

### Instructions:

Write a program that prints the Latin alphabet in lowercase on a single line.

### Usage:

```py
% python3 printalphabet.py 
abcdefghijklmnopqrstuvwxyz
%
```


## PrintReversAlphabet

### Instructions:

Write a program that prints the Latin alphabet in lowercase in reverse order (from 'z' to 'a') on a single line.

### Usage:

```py
% python3 printReversAlphabet.py 
zyxwvutsrqponmlkjihgfedcba
%
```


## PrintDigits

### Instructions:

Write a program that prints the decimal digits in ascending order (from 0 to 9) on a single line.

### Usage:

```py
% python3 printdigits.py 
0123456789
%
```


## IsNegative

### Instructions:

Write a function that prints 'T' (true) on a single line if the int passed as parameter is negative, otherwise it prints 'F' (false).

# Expected function:

```py
def IsNegative(nb):
```
### Usage:

```py
def main() :
    IsNegative(1)
    IsNegative(0)
    IsNegative(-1)
```
### And its output : 

 ```py
 % python3 isnegative.py
 F
 F
 T
 %
 ```


## Printcomb

### Instructions

Write a function that prints, in ascending order and on a single line: all unique combinations of three different digits so that, the first digit is lower than the second, and the second is lower than the third. These combinations are separated by a comma and a space.

### Expected function:

```py
def PrintComb():
```

### Usage:

Here is a possible program to test your function :
```py
def main() :
    Printcomb()
```

### And its output :

```py
% python3 printcomb.py
012, 013, 014, 015, 016, 017, 018, 019, 023, ..., 689, 789$
%
```
### Info:
000 or 999 are not valid combinations because the digits are not different.
987 should not be shown because the first digit is not less than the second.


## Printcomb2

### Instructions:

Write a function that prints in ascending order and on a single line: all possible combinations of two different two-digit numbers.<br/>
These combinations are separated by a comma and a space.

### Expected function:

```py
def PrintComb2():
```

### Usage:

Here is a possible program to test your function :
```py
def main() :
    Printcomb2()
```

### And its output :
```py
% python3 printcomb.py
00 01, 00 02, 00 03, ..., 00 98, 00 99, 01 02, 01 03, ..., 97 98, 97 99, 98 99$
%
```


## PrintCombN

### Instructions:

Write a function that prints all possible combinations of n different digits in ascending order.
n will be defined as : 0 < n < 10<br/>
Below are the references for the printing format expected.<br/>
(for n = 1) '0, 1, 2, 3, ..., 8, 9'<br/>
(for n = 3) '012, 013, 014, 015, 016, 017, 018, 019, 023,...689, 789'

### Expected function:

```py
def PrintCombN(n: int):
```

### Usage:

Here is a possible program to test your function :
```py
def main() :
    PrintCombN(1)
    PrintCombN(3)
    PrintCombN(9)
```

### And its output :

```py
% python3 printcomb.py
0, 1, 2, 3, 4, 5, 6, 7, 8, 9
012, 013, 014, 015, 016, 017, 018, ... 679, 689, 789
012345678, 012345679, ..., 123456789
%
```
**Warning !!! -->   Be mindful of your program efficiency to avoid timeouts..**
